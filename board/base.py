import collections
import dataclasses
import typing


@dataclasses.dataclass()
class Field:
    x: int
    y: int

    @staticmethod
    def parse_fields(fields: typing.List[str]) -> typing.List['Field']:
        fields = [field.split(',') for field in fields]
        fields = [Field(int(x), int(y)) for x, y in fields]
        return fields

    def __hash__(self):
        return hash((self.x, self.y))


@dataclasses.dataclass()
class User:
    pid: int
    name: str
    skin: str
    skinned: str

    field: Field
    score: int = 0


class Board:
    def __init__(self, height: int, width: int):
        self.height = height
        self.width = width

        self.users = {}
        self.occupied = collections.defaultdict(set)
        self.paths = collections.defaultdict(set)

    def show_board(self) -> None:
        pass

    def add_user(self, user: User, occ: typing.List[Field], path: typing.List[Field]) -> None:
        self._add_occupation(user, occ)
        self._add_path(user, path)
        self._place_user(user, user.field)

    def move_user(self, pid: int, field: Field, occ: typing.List[Field]) -> None:
        user = self.users[pid]

        if occ:
            self._remove_path(user)
            self._add_occupation(user, occ)
        elif field not in self.occupied[user.pid]:
            self._add_path(user, [field])

        self._place_user(user, field)

    def kill_user(self, pid: int) -> None:
        user = self.users[pid]

        self._remove_occupation(user)
        self._remove_path(user)
        del self.users[pid]

    def _place_user(self, user: User, field: Field):
        self.users[user.pid] = user
        user.field = field

    def _add_occupation(self, user: User, occ: typing.List[Field]) -> None:
        self.occupied[user.pid] = self.occupied[user.pid].union(set(occ))

    def _remove_occupation(self, user: User) -> None:
        del self.occupied[user.pid]

    def _add_path(self, user: User, path: typing.List[Field]) -> None:
        self.paths[user.pid] = self.paths[user.pid].union(set(path))

    def _remove_path(self, user: User) -> None:
        del self.paths[user.pid]
