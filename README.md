## Paper.io client

Custom asynchronous client for paper.io online game, 
designed for the purpose of human interaction and writing bots.

#### ISSUES

Uploaded version is not currently working due to changes on server game sites, 
which broke current implementation of communication protocol.
