import dataclasses
import json
import typing

import pygame
import websockets

from board.base import Board, Field, User
from board.visual import VisualBoard, VisualUser


@dataclasses.dataclass
class Message:
    msg_type: str
    msg_data: dict

    def serialize(self) -> str:
        return f'{self.msg_type} {json.dumps(self.msg_data)}'

    @classmethod
    def deserialize(cls, msg: str) -> typing.List['Message']:
        msg_type, msg_data = msg.split(' ', 1)
        msg_data = json.loads(msg_data)

        if msg_type == 'MULTI':
            return [Message(_msg_type, _msg_data) for _msg_type, _msg_data in msg_data]
        return [Message(msg_type, msg_data)]


class BaseClient:
    BOARD_CLASS = Board
    USER_CLASS = User

    HANDLERS = {}

    def __init__(self) -> None:
        self.socket = None
        self.board = None
        self.user_pid = None

    @property
    def on(self):
        return self.socket and self.socket

    @property
    def off(self):
        return not self.on

    async def connect(self, server: str) -> None:
        if self.off:
            self.socket = await websockets.connect(server)

    async def close(self) -> None:
        if self.on:
            await self.socket.close()
            self.socket = None

    async def send_message(self, msg: Message) -> None:
        serialized_msg = msg.serialize()
        await self.socket.send(serialized_msg)

    async def recv_messages(self) -> typing.List[Message]:
        serialized_msg = await self.socket.recv()
        return Message.deserialize(serialized_msg)

    async def initialize(self, server: str) -> None:
        await self.connect(server)
        init_msg = Message('PLAY', {'username': 'PaperBot', 'party': '0', 'skinny': '0'})
        await self.send_message(init_msg)

    async def process_messages(self, messages) -> None:
        for msg in messages:
            await self.__getattribute__(self.HANDLERS[msg.msg_type])(msg.msg_data)

    async def visualize(self) -> None:
        if self.board:
            self.board.show_board()


class GameClient(BaseClient):
    BOARD_CLASS = VisualBoard
    USER_CLASS = VisualUser

    HANDLERS = {
        'INITROOM': '_init_room_handler',
        'FULLROOM': '_game_over_handler',
        'INITPLAYER': '_init_player_handler',
        'SPAWN': '_spawn_handler',
        'MOVE': '_move_handler',
        'SCORES': '_scores_handler',
        'KILL': '_kill_handler',
        'GAMEOVER': '_game_over_handler',
    }

    KEY_MAPPING = {
        pygame.K_LEFT: 37,
        pygame.K_UP: 38,
        pygame.K_RIGHT: 39,
        pygame.K_DOWN: 40,
    }

    async def initialize(self, server: str) -> None:
        await self.connect(server)
        init_msg = Message('PLAY', {'username': 'PaperBot', 'party': '0', 'skinny': '0'})
        await self.send_message(init_msg)

    async def _init_room_handler(self, data: dict) -> None:
        self.board = self.BOARD_CLASS(width=data['width'], height=data['height'])
        self.user_pid = data['pid']

    async def _game_over_handler(self, _: dict) -> None:
        await self.close()

    async def _init_player_handler(self, data: dict) -> None:
        fields = Field.parse_fields(data['pg'])
        path = Field.parse_fields(data['pp'])
        user = self.USER_CLASS(data['pid'], data['name'], data['skin'], data['skinned'], Field(data['x'], data['y']))
        self.board.add_user(user=user, occ=fields, path=path)

    async def _spawn_handler(self, data: dict) -> None:
        user = self.USER_CLASS(data['pid'], data['name'], data['skin'], data['skinned'], Field(data['x'], data['y']))
        fields = [
            Field(x, y)
            for x in range(user.field.x - 1, user.field.x + 2)
            for y in range(user.field.y - 1, user.field.y + 2)
        ]
        self.board.add_user(user, fields, [])

    async def _move_handler(self, data: dict) -> None:
        fields = Field.parse_fields(data['d']) if isinstance(data['d'], list) else []
        self.board.move_user(pid=data['pid'], field=Field(data['x'], data['y']), occ=fields)

    async def _scores_handler(self, data: dict) -> None:
        pass

    async def _kill_handler(self, data: dict) -> None:
        self.board.kill_user(pid=data['pid'])

    async def handle_input(self) -> None:
        pressed_key = None

        try:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    await self.close()
                    return

                if event.type == pygame.KEYDOWN and event.key in self.KEY_MAPPING:
                    pressed_key = self.KEY_MAPPING[event.key]
        except pygame.error:
            pass

        if pressed_key:
            await self.send_message(Message('KEYPRESS', {'key': pressed_key}))
